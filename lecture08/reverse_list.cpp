// reverse_list.cpp

#include <iostream>
#include <memory>
#include <stack>

using namespace std;

// Node Structure

template <typename T>
struct Node {
    T data;
    Node *next;

    ~Node() {
    	if (next) delete next;
    }
};

// Print Utility

template <typename T>
void print_list(Node<T> *head, const char *prefix) {
    cout << prefix;
    for (Node<T> *curr = head; curr != nullptr; curr = curr->next) {
    	cout << " " << curr->data;
    }
    cout << endl;
}

// Reverse List: Recursive

template <typename T>
Node<T> * reverse_node(Node<T> *curr, Node<T> *prev) {
    Node<T> *tail = curr;

    if (curr->next != nullptr) {
    	tail = reverse_node(curr->next, curr);
    }

    curr->next = prev;
    return tail;
}

template <typename T>
Node<T> * reverse_list(Node<T> *head) {
    return reverse_node(head, static_cast<Node<T> *>(nullptr));
}

// Reverse List: Iterative

template <typename T>
Node<T> * reverse_list_iter(Node<T> *head) {
    Node<T> *next, *tail = nullptr;

    for (auto curr = head; curr != nullptr; curr = next) {
    	next = curr->next;
    	curr->next = tail;
    	tail = curr;
    }

    return tail;
}

// Main execution

int main(int argc, char *argv[]) {
    Node<int> list = Node<int>{
    	0, new Node<int>{
    	1, new Node<int>{
    	2, new Node<int>{
    	3, new Node<int>{
    	4, nullptr
    }}}}};
    Node<int> *head = &list;

    print_list(head, "Original:");

    head = reverse_list(head);
    print_list(head, "Reversed:");
    
    head = reverse_list_iter(head);
    print_list(head, "Reversed:");

    return 0;
}
