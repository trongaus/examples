// multi_stable.cpp

#include <algorithm>
#include <iostream>
#include <string>

using namespace std;

struct Student {
    string first_name;
    string last_name;
};

const size_t NSTUDENTS = 6;

Student students[NSTUDENTS] = {
    {"Steve" , "Nguyen"},
    {"Phu"   , "Nguyen"},
    {"Jack"  , "Johnson"},
    {"Chris" , "Johnson"},
    {"Robert", "Smith"},
    {"John"  , "Smith"},
};

void print_students(Student a[], size_t n) {
    for_each(a, a + n, [](Student &s) { cout << s.last_name << ", " << s.first_name << endl; });
    cout << endl;
}

bool compare_last_names(const Student &a, const Student &b) {
    return a.last_name < b.last_name;
}

bool compare_first_names(const Student &a, const Student &b) {
    return a.first_name < b.first_name;
}

int main(int argc, char *argv[]) {
    print_students(students, NSTUDENTS);

    stable_sort(students, students + NSTUDENTS, compare_first_names);
    print_students(students, NSTUDENTS);

    stable_sort(students, students + NSTUDENTS, compare_last_names);
    print_students(students, NSTUDENTS);

    return 0;
}
