// map.cpp

#include <algorithm>
#include <iostream>
#include <map>
#include <string>

using namespace std;

int main(int argc, char *argv[]) {
    map<string, string> heroes = {
	{"deadpool", "Marvel"},
	{"batman"  , "DC"},
	{"olaf"    , "Disney"},
    };

    cout << "Size:               " << heroes.size() << endl;

    cout << "Elements:          "; 
    for (auto p : heroes) {
    	cout << " (" << p.first << ", " << p.second << ")";
    }
    cout << endl;

    cout << "Search(deadpool):   " 
    	 << (heroes.find("deadpool") != heroes.end() ? "Found" : "Not Found") << endl;
    
    cout << "Search(spider-man): " 
    	 << (heroes.find("spider-man") != heroes.end() ? "Found" : "Not Found") << endl;

    cout << heroes["deadpool"] << endl;
    heroes["deadpool"] = "Dark Horse";
    cout << heroes["deadpool"] << endl;

    return 0;
}
