// makefile_tsort.cpp

#include <iostream>
#include <functional>
#include <map>
#include <map>
#include <queue>
#include <string>
#include <set>
#include <sstream>
#include <vector>

#include <boost/algorithm/string/trim.hpp>

using namespace std;

// Structures

struct Graph {
    void load_graph(istream &is) {
        string line;

        // Clear Graph
        edges.clear();

        // Read rules
        while (getline(is, line)) {
            // Look for rules containing :
            size_t split = line.find(':');
            if (split == string::npos) {
                continue;
            }

            // Split into target and sources substrings
            string target  = line.substr(0, split);
            string sources = line.substr(split + 1, line.size() - split);

            // Trim substrings
            boost::trim(target);
            boost::trim(sources);

            // Add each source to adjacency list
            stringstream ss(sources);
            string source;
            while (ss >> source) {
                // Initialize edges
                if (!edges.count(source)) {
                    edges[source] = vector<string>();
                }

                // Initialize degrees
                if (!degrees.count(source)) {
                    degrees[source] = 0;
                }

                // Add edge
                edges[source].push_back(target);

                // Increment degree
                degrees[target] += 1;
            }
        }
    }

    void topological_sort(vector<string> &sorted) {
        sorted.clear();
        queue<string> frontier;
        //priority_queue<string> frontier;
        //priority_queue<string, vector<string>, greater<string>> frontier;

        // Add nodes with zero degree
        for (auto &p : degrees)
            if (p.second == 0)
                frontier.push(p.first);

        // Explore frontier
        while (!frontier.empty()) {
            // Pop off a node
            auto n = frontier.front(); frontier.pop();

            // Store visited node
            sorted.push_back(n);

            // Remove edge by decrementing target degree
            for (auto &v : edges[n]) {
                degrees[v] -= 1;

                // If target degree is 0, then add to frontier
                if (degrees[v] == 0)
                    frontier.push(v);
            }
        }
    }

    map<string, size_t>             degrees;
    map<string, vector<string>>     edges;
};

// Main Execution

int main(int argc, char *argv[]) {
    Graph g;
    vector<string> sorted;

    g.load_graph(cin);
    g.topological_sort(sorted);

    if (sorted.size() != g.edges.size()) {
        cerr << "Graph contains a cycle!" << endl;
        return EXIT_FAILURE;
    }

    for (auto &v : sorted) {
        cout << v << endl;
    }

    return EXIT_SUCCESS;
}

// vim: set sts=4 sw=4 ts=8 expandtab ft=cpp:
