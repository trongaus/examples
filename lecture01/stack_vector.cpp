// stack_vector.cpp

#include <stack>
#include <vector>

using namespace std;

const int N = 1<<28;

int main(int argc, char *argv[]) {
    stack<int, vector<int>> s;

    // Fill stack
    for (int i = 0; i < N; i++) {
    	s.push(i);
    }

    // Empty stack
    while (!s.empty()) {
    	s.pop();
    }

    return 0;
}
