#include <random>
#include <unordered_set>

using namespace std;

#define NUMBERS 1<<25

int main(int argc, char *argv[]) {
    unordered_set<int> s;
    default_random_engine e;
    uniform_int_distribution<uint32_t> d;

    for (size_t i = 0; i < NUMBERS; i++) {
    	s.insert(d(e) % NUMBERS);
    }
    
    for (size_t i = 0; i < NUMBERS; i++) {
    	s.find(i);
    }

    return 0;
}
