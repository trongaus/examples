#include <bitset>
#include <iostream>

using namespace std;

int main(int argc, char *argv[]) {
    bitset<8> s = 0;

    // Set 6th bit
    s.set(6);
    cout << s << endl;

    // Test 6th bit
    if (s.test(6)) {
    	cout << "Set!" << endl;
    }

    // Clear 6th bit
    s.reset(6);
    cout << s << endl;
    
    // Test 6th bit
    if (s.test(6)) {
    	cout << "Set!" << endl;
    }

    return 0;
}
