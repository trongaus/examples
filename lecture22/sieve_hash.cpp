// sieve_hash.cpp

#include <cmath>
#include <cstdint>
#include <iostream>
#include <unordered_set>

const size_t N = 1<<20;

using namespace std;

int main(int argc, char *argv[]) {
    unordered_set<size_t> s;

    // Initialize numbers
    for (size_t i = 1; i < N; i++) {
        s.insert(i);
    }

    // Sieve
    for (size_t i = 2; i < (size_t)(sqrt(N)); i++) {
        if (s.count(i)) {
            for (size_t p = i*i; p < N; p += i) {
                s.erase(p);
            }
        }
    }
    
    // Output
    for (auto v : s) {
        cout << v << endl;
    }

    return 0;
}

// vim: set sts=4 sw=4 ts=8 expandtab ft=cpp:
