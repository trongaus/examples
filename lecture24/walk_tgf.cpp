// walk_tgf.cpp: read TGF and walk it

#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

// WalkType Constants

typedef enum {
    DFS_REC,
    DFS_ITER,
    BFS_ITER,
} WalkType;

// Graph structure

template <typename NodeLabel, typename EdgeLabel>
struct Graph {
    vector <NodeLabel> labels;
    vector <map<int, EdgeLabel>> edges;
};

// Load graph from standard input

template <typename NodeLabel, typename EdgeLabel>
void load_graph(Graph<NodeLabel, EdgeLabel> &g) {
    string line;

    // Clear Graph
    g.labels.clear();
    g.edges.clear();

    // Read labels (vertices)
    g.labels.push_back(""); // TGF starts nodes at 1, so add dummy node

    while (getline(cin, line) && line[0] != '#') {
        stringstream ss(line);
        int node;
        string label;

        ss >> node;
        getline(ss, label);
        g.labels.push_back(label);
    }

    // Read edges (vertices)
    g.edges.resize(g.labels.size());

    while (getline(cin, line)) {
        stringstream ss(line);
        int source;
        int target;
        string label;

        ss >> source >> target;
        getline(ss, label);

        g.edges[source][target] = label;
    }
}

// Walk graph dispatch function

template <typename NodeLabel, typename EdgeLabel>
void walk_graph(Graph<NodeLabel, EdgeLabel> &g, int root, WalkType w) {
    set<int> marked;

    switch (w) {
        case DFS_REC:
            walk_graph_dfs_rec(g, root, marked);
            break;
        case DFS_ITER:
            walk_graph_dfs_iter(g, root);
            break;
        case BFS_ITER:
            walk_graph_bfs_iter(g, root);
            break;
        default:
            cerr << "Unknown WalkType: " << w << endl;
            break;
    }
}

// Depth-First-Search (recursive)

template <typename NodeLabel, typename EdgeLabel>
void walk_graph_dfs_rec(Graph<NodeLabel, EdgeLabel> &g, int v, set<int> &marked) {
    // TODO
}

// Depth-First-Search (iterative)

template <typename NodeLabel, typename EdgeLabel>
void walk_graph_dfs_iter(Graph<NodeLabel, EdgeLabel> &g, int v) {
    // TODO
}

// Breadth-First-Search (iterative)

template <typename NodeLabel, typename EdgeLabel>
void walk_graph_bfs_iter(Graph<NodeLabel, EdgeLabel> &g, int v) {
    // TODO
}

// Main execution

int main(int argc, char *argv[]) {
    Graph<string, string> g;

    if (argc != 3) {
        cerr << "usage: " << argv[0] << " root [0 = DFS_REC | 1 = DFS_ITER | 2 = BFS_ITER]" << endl;
        return EXIT_FAILURE;
    }

    load_graph(g);
    walk_graph(g, static_cast<size_t>(atoi(argv[1])), static_cast<WalkType>(atoi(argv[2])));

    return EXIT_SUCCESS;
};
